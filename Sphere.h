// From http://www.glprogramming.com/red/chapter02.html#name8

#ifndef SPHERE_H
#define SPHERE_H
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include "Point3.h"

using std::cout;
using std::endl;

class Sphere
{
    private:
        // icosahedron vertices
        static Point3d vertices[12];

        // icosehedron faces
        static int triangle_indexes[20][3];

        GLfloat _angle;

        int _subdiv;
        bool _animation;

        // Subdivide each face by depth
        void subdivide(Point3d p1, Point3d p2, Point3d p3, int depth);

    public:

        Sphere(const int subdiv);

        //Draw the sphere
        void display(void);

        // Glut BS: not used
        void reshape(int w, int h);

        // Glut BS: not used
        void idle();
};

#endif // SPHERE_H