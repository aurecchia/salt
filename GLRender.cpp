/****************************************************************************
 ** simple image viewer using qt
 ****************************************************************************/

#include <QtGui>
#include "GLRender.h"
#include <iostream>


GLRender::GLRender(QWidget *parent,Qt::WindowFlags flags)
: QMainWindow(parent, flags)
{
    setupUi(this);
}

GLRender::~GLRender()
{
    //if (canvas) delete canvas;
}

 void GLRender::keyPressEvent(QKeyEvent *k)
 {
     canvas->keyPressEvent(k);
 }


 void GLRender::keyReleaseEvent(QKeyEvent *k)
 {
     canvas->keyReleaseEvent(k);
 }



