﻿#ifndef _ORBITING_CAMERA_H
#define _ORBITING_CAMERA_H

#include "Planet.h"
#include "Point3.h"

class OrbitingCamera
{
    private:
        /** The step by which the camera moves. */
        float _mov_step;

        /** The step by which the camera turns. */
        float _turn_step;

        /** The step by which the camera zooms. */
        float _zoom_step;

        /** The eye vector (camera position). */
        Point3d _eye;

        /** The center of attention. */
        Point3d _center;

        /** Up vector (to the sky). */
        Point3d _up;

        /**
         *  Tells whether the camera is moving.
         *
         *  Set to -1, 0 or 1 when the camera is moving backwards, not moving
         *  or is moving forwards.
         */
        float _moving;

        /**
         *  Tells whether the camera is strafing (left or right).
         *
         *  Set to -1, 0 or 1 when the camera is strafing left, not strafing or
         *  is strafing right.
         */
        float _strafing;

        /**
         *  Tells whether the camera is zooming.
         *
         *  Set to -1, 0 or 1 when the camera is zooming forwards, not zooming
         *  or zooming backwards.
         */
        float _zooming;

        /** The planet the camera is walking on. */
        Planet _planet;

        /** The factor (wrt the planet radius) at which the camera orbits. */
        float _orbiting_factor;

        /** the current zoom level (between 0.0 and 1.0). */
        float _zoom_level;

        /** The effective zoom range. */
        float _zoom_range;

        /** Multiplication between 4x4 column major matrix and a 3x1 vector. */
        Point3d _mult(const float *mat, Point3d &vec);

        /** Updates the 3 camera vectors with the given matrix. */
        void _update_vectors(float matrix[]);

        /** Moves the camera vectors. */
        void _update_move();

        /** Strafes the camera vectors. */
        void _update_strafe();

        /** Updates the zoom factor. */
        void _update_zoom();


    public:
        /** Constructor for the camera. */
        OrbitingCamera(const Planet &planet);

        /** Sets the movement flag (-1 is forwards, 0 stop, 1 backwards). */
        void move(const float flag);

        /** Sets the strafe flag (-1 is left, 0 stop, 1 right). */
        void strafe(const float flag);

        /** Sets the strafe flag (-1 is forwards, 0 stop, 1 backwards). */
        void zoom(const float flag);

        /** Checks the flags for updates and positions the camera. */
        void look();
};
#endif
