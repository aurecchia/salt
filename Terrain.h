﻿#ifndef TERRAIN_H
#define TERRAIN_H

#include <QtOpenGL>
#include "Base.h"
#include "Point2.h"
#include "utils/noiseutils.h"
#include "Color.h"


class Terrain
{
    private:
        /** The width of the terrain. */
        unsigned int _width;

        /** The width of the terrain. */
        unsigned int _height;

        /** The height-map of the terrain. */
        utils::NoiseMap _height_map;

        /** Initializes the terrain. */
        void initialize();

        /** Image of the heightmap in gl format */
        QImage _qimage;

    public:
        /**
         * Constructor for a terrain of the given size.
         *
         * @param width  : The width of the terrain.
         * @param height : The height of the terrain.
         */
        Terrain(const unsigned int &width, const unsigned int &height);

        /**
         * Returns the image of the heightmap in gl format.
         * 
         * @return The image in gl format.
         */
        QImage* height_map();


        /**
         * Returns the width of the terrain.
         *
         * @return The width.
         */
        int width();


        /**
         * Returns the height of the terrain.
         *
         * @return The width.
         */
        int height();


        /**
         * Returns the height of the terrain at the given coordinates.
         *
         * @param x : The x coordinate where to get the height.
         * @param y : The y coordinate where to get the height.
         *
         * @return The height of the terrain at coordinates <x, y>.
         */
        float level(const unsigned int x, const unsigned int y);


        /**
         * Sets the material in OpenGL for the terrain at given coordinates.
         *
         * @param x : The x coordinate on the terrain.
         * @param y : The y coordinate on the terrain.
         */
        void setMaterial(const unsigned int x, const unsigned int y);


        /**
         * Converts coordinates from geographical to cartesian system.
         *
         * @param latitude  : The latitude (vert) coordinate.
         * @param longitude : The longitude (horz) coordinate.
         *
         * @return 
         */
        Point2d to_cartesian(const float latitude, const float longitude);
};

#endif // TERRAIN_H
