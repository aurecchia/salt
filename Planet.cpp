#include "Planet.h"
#include "Base.h"
#include <math.h>


Planet::Planet(const int &lats, const int &longs, const int &scale)
:   _lats( lats ), _longs( longs ), _scale( scale ),
    _terrain( 1024, 512 ), _deformation( 0.0 )
{
    build();
}


void Planet::build()
{
    // Compute length of each side-strip
    float step = 2 * PI / _longs;

    // Build strips
    for(int i = 0; i < _longs; ++i)
        buildSegment(i, step);
}

QImage* Planet::height_map() {
    return _terrain.height_map();
}


void Planet::buildSegment(const int &index, const float &phiStep)
{
    // Sides of the strip
    float phis[] = { index * phiStep, (index + 1) * phiStep };

    // Add the vertex and texture arrays to their vectors
    strips.push_back(PointArray());
    textures.push_back(TextureArray());
    normals.push_back(PointArray());

    // Get last elements from vectors to actually add the data
    PointArray &segment = strips.back();
    TextureArray &seg_textures = textures.back();
    PointArray &seg_normals = normals.back();

    Point3d point = Point3d( 0.0, 0.0, 1.0 );
    Point2d texture_coords = Point2d(0.0f, 0.0f);

    // Create point on "south pole"
    set(point, texture_coords, segment, seg_textures, seg_normals);

    // For all latitude steps
    float step = PI / _lats;
    for(int lat_index = 1; lat_index < _lats; ++lat_index)
    {
        const float latitude = lat_index * step;

        // For the 2 longitudes on this strip
        for(int lon_index = 0; lon_index < 2; ++lon_index)
        {
            const float longitude = phis[lon_index];

            point = Point3d (
                    sin(latitude) * cos(longitude),
                    sin(latitude) * sin(longitude),
                    cos(latitude)
                    );

            float texture_x;
            if (lat_index == 0 || lat_index == _lats - 1)
                texture_x = 0.0f;
            else
                texture_x = 1.0f * (index + lon_index) / _longs;

            const float texture_y = 1.0f * lat_index / _lats;
            texture_coords = Point2d(texture_x, texture_y);

            // Create point at the current <lat, long> coordinate
            set(point, texture_coords, segment, seg_textures, seg_normals);
        }
    }

    // Create terrain point for bottom "north pole"
    texture_coords = Point2d(1.0f, 1.0f);
    point = Point3d( 0.0, 0.0, -1.0 );

    set(point, texture_coords, segment, seg_textures, seg_normals);
}


void Planet::set(const Point3d &point,
                const Point2d &txt_coord,
                PointArray &segment,
                TextureArray &txt,
                PointArray &seg_normals) const
{
    segment.push_back(point);
    assert(txt_coord.x()>=0 && txt_coord.x()<=1);
    seg_normals.push_back( point.normalized() );
    txt.push_back(txt_coord);
}


float Planet::height_at(const float theta, const float phi)
{
    const int width = _terrain.width();
    const int height = _terrain.height();
    const unsigned int x = (int) (( theta / (2 * PI) ) * width ) % width;
    const unsigned int y = (int) (( phi / PI ) * height) % height;
    return _terrain.level(x, y);
}


int Planet::scale() const
{
    return _scale;
}


void Planet::draw()
{
    glScalef( _scale, _scale, _scale );
    for(unsigned int i = 0; i < strips.size(); ++i)
    {
        const PointArray &segment = strips[i];
        const TextureArray &txt = textures[i];
        const PointArray &seg_normals = normals[i];


        glBegin(GL_TRIANGLE_STRIP);
        unsigned int j;
        for(j = 0; j < segment.size(); ++j)
        {
            const Point3d &p = segment[j];
            const Point2d &t= txt[j];
            const Point3d &n = seg_normals[j];

            glNormal3d(n.x(), n.y(), n.z());
            glTexCoord2d(t.x(), t.y());
            glVertex3d(p.x(), p.y(), p.z());
        }
        glEnd();
    }
}


Point2d Planet::to_spherical(const Point3d point)
{
    const float radius = sqrt(
            point[0] * point[0] +
            point[1] * point[1] +
            point[2] * point[2]
            );
    const float lon = fmod( atan2(point.y(), point.x()) + (2*PI),2*PI);
    const float lat = acos( point.z() / radius );
    return Point2d( lon, lat );
}


